import xs from 'xstream';
import sampleCombine from 'xstream/extra/sampleCombine'
import { run } from '@cycle/xstream-run';
import { makeDOMDriver, h1, div, input, span, label, select, option, a, p } from '@cycle/dom';
import isolate from '@cycle/isolate';
import acCardPrinter from '@agilecards/card-printer';
import acCardQueue from '@agilecards/issue-queue';
import style from './index.css';

const createInput = sources => isolate(({ DOM, props }) => {
    return {
        DOM: xs.of(div('.' + style.inputContainer, [
            label('.' + style.inputLabel, [
                props.title,
                span('.' + style.inputLabelSubtitle, props.subtitle)
            ]),
            input('.' + style.input, { props: { type: props.type || 'text', value: props.value } }, [])
        ])),
        value$: DOM.select('input').events('change').map(e => e.target.value).startWith(props.value)
    };
})(sources);

const createSelect = sources => isolate(({ DOM, props }) => {
    return {
        DOM: xs.of(div('.' + style.inputContainer, [
            label('.' + style.inputLabel, [
                props.title,
                span('.' + style.inputLabelSubtitle, props.subtitle)
            ]),
            select('.' + style.input, props.options
                .map(value => option({
                    props: {
                        value
                    }
                }, value)))
        ])),
        value$: DOM.select('select').events('change').map(e => e.target.value).startWith(props.options[0])
    };
})(sources);

const createForm = ({ DOM }) => {
    const jiraInput = createInput({
        DOM,
        props: {
            title: 'JIRA URL',
            value: 'https://00000000.atlassian.net'
        }
    });

    const jiraUser = createInput({
        DOM,
        props: {
            title: 'Username',
            value: 'admin'
        }
    });

    const jiraPass = createInput({
        DOM,
        props: {
            title: 'Password',
            value: 'Admin123',
            type: 'password'
        }
    });

    const printerName = createSelect({
        DOM,
        props: {
            title: 'Printer',
            options: acCardPrinter.getPrinterNames()
        }
    });

    const sprintId = createInput({
        DOM,
        props: {
            title: 'Sprint ID',
            value: 1
        }
    });

    const buttonVisibility = DOM.select('.run').events('click').mapTo(false).startWith(true);

    const runButton$ = buttonVisibility.map(visible => {
        return visible ? a(`.${style.button}.run`, 'Run') : p(`.${style.info}`, 'Running...')
    });

    const formElements$ = xs.combine(
        jiraInput.DOM,
        jiraUser.DOM,
        jiraPass.DOM,
        printerName.DOM,
        sprintId.DOM,
        runButton$
    );

    const formData$ = xs.combine(
        jiraInput.value$,
        jiraUser.value$,
        jiraPass.value$,
        printerName.value$,
        sprintId.value$)
        .map(([jira, user, pass, printer, sprintId]) => ({
            jira,
            user,
            pass,
            printer,
            sprintId
        }));

    return {
        DOM: formElements$.map(elements => div(`.${style.form}`, elements)),
        data: DOM.select('.run').events('click').compose(sampleCombine(formData$))
            .map(([, formData]) => formData)
    }
};

const printIssues = printerOptions => issues => {
    console.log(`Printing issues: ${issues.map(issue => issue.key).join(', ')}`);
    if (issues.length > 0) {
        acCardPrinter.print(issues, {
            rendererUrl: 'http://localhost:8080',
            pdfOptions: {
                marginsType: 1,
                pageSize: {
                    width: 62000,
                    height: 100000
                }
            },
            printer: printerOptions
        }).catch(console.error)
    }
};

const runQueue = (jira, sprintId, printerOptions) => {
    console.log(`Polling started on ${jira.url} for sprint ${sprintId}`);
    acCardQueue.pollIssues(jira, sprintId, printIssues(printerOptions));
};

function main({ DOM }) {
    const form = createForm({ DOM });

    form.data.addListener({
        next: data => runQueue({
            url: data.jira,
            user: data.user,
            pass: data.pass
        }, data.sprintId, {
            name: data.printer,
            pageSize: '62mm'
        })
    });

    const sinks = {
        DOM: xs.combine(form.DOM).map(([dom]) => div([
            dom
        ]))
    };
    return sinks;
}

const drivers = {
    DOM: makeDOMDriver('#app')
};

run(main, drivers);
